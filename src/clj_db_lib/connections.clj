(ns clj-db-lib.connections
  (:require [clj-data-lib.data :refer [map->qualified qualified-map->unqualified]]
            [clj-db-lib.provider :as p]
            [clj-db-lib.spec.connections :as c]
            [clj-db-lib.utils.exceptions :refer [validation-error]]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [expound.alpha :refer [expound-str]]
            [hikari-cp.core :as hc]
            [taoensso.timbre :as log])
  (:import (com.zaxxer.hikari HikariConfig HikariDataSource)))

(defn- conn-properties->db-spec
  "Return db-spec with appropriate defaults for input connection properties."
  [{:keys [::c/read_only ::c/port ::c/user ::c/pswd ::c/session-queries ::c/idle-timeout
           ::c/connection-timeout ::c/maximum-pool-size ::c/minimum-idle] :as properties}]
  (let [sqs (or session-queries (p/session-queries properties))
        pswd-or-token (or (p/db-token properties) pswd)]
    (merge {:idle-timeout       (or idle-timeout (* 3 60 60))
            :connection-timeout (or connection-timeout 20000)
            :maximum-pool-size  (or maximum-pool-size 10)
            :minimum-idle       (or minimum-idle 3)}
           (p/db-spec properties)
           (when read_only {:read-only true})
           (when port {:port-number port})
           (when (and user pswd-or-token) {:username user :password pswd-or-token})
           (when (pos? (count sqs))
             {:connection-init-sql (string/join "; " sqs)}))))

;(defn- pool [spec] (hc/make-datasource spec))

(defn- pool
  [spec properties ds-props]
  (let [^HikariConfig ds-config (hc/datasource-config spec)]
    (doseq [[prop val] (merge (p/add-ds-props properties) ds-props)]
      (log/debug prop " " val)
      (.addDataSourceProperty ds-config (name prop) val))
    (HikariDataSource. ds-config)))

(defprotocol Connection
  (close [this]))

(defrecord DBPooledSpec [datasource]
  Connection
  (close [_]
    (log/debug "Closing connection.")
    (hc/close-datasource datasource)))

(defn qualify-connection-properties [properties]
  (let [qm (map->qualified ::c/db-connection properties)]
    (when-not (s/valid? ::c/db-connection qm)
      (throw (validation-error "Invalid DB connection spec" (expound-str ::c/db-connection qm))))
    qm))

(defn- update-conn-props [properties]
  (update-in properties [:provider] keyword))

(defn db-pooled-spec
  "Return pooled connection spec"
  [properties]
  (log/merge-config! {:ns-blacklist ["com.zaxxer.hikari.*"]})
  (let [qp (-> properties qualified-map->unqualified update-conn-props qualify-connection-properties)
        ds-props (-> properties qualified-map->unqualified :ds-props)]
    (log/debug "Set system properties if required by the DB connection.")
    (doseq [[prop val] (p/system-props qp)]
      (System/setProperty prop val))
    (-> (conn-properties->db-spec qp)
        (pool qp ds-props)
        (DBPooledSpec.))))
