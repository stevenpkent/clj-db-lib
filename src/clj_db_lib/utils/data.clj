(ns clj-db-lib.utils.data
  (:require [clj-data-lib.string :as cs]
            [clj-data-lib.translate :as trn]
            [clj-db-lib.utils.exceptions :as e]
            [clj-db-lib.provider :as p]
            [clojure.data.csv :as csv]
            [clojure.string :as string]
            [honeysql-plus.core :as hh+]))

(defn valid? [sample-row columns]
  (when-let [field-count (and (not-empty sample-row)
                              (not-empty columns)
                              (if (map? sample-row)
                                (count (keys sample-row))
                                (count sample-row)))]
    (when (not= (count columns) field-count)
      (throw (e/validation-error
              "Number columns does not match field count."
              (str "Field count: " field-count ", columns: " columns))))))

(defn quote-identifiers
  "HoneySQL-plus conditional quoting on fields. Used for inserts/updates/etc.

   opts:
   - provider
   - quote-when-fn (optional)
   "
  [v opts]
  (let [hh-opts (p/hh-opts opts)]
    (cond
      (keyword? v) (hh+/sql-format-quoting-field v hh-opts)
      (map? v) (into {} (map (fn [[k1 v1]] {(hh+/sql-format-quoting-field k1 hh-opts) v1}) v))
      (coll? v) (vec (map #(hh+/sql-format-quoting-field % hh-opts) v))
      :else v)))

(defn as-arrays? [{:keys [reducer-fn]}]
  (nil? reducer-fn))

(defn- clean-regex
  "Create regular expression pattern"
  [& separators]
  (re-pattern (str "[\n\t\r\f" (string/join "" separators) "]")))

(defn- shorten-str [v max-str-bytes]
  (if max-str-bytes (cs/shorten-utf8 v max-str-bytes) v))

(defn- recode-string
  "Transform string from source encoding to target encoding."
  [target source s]
  (if (or (nil? source) (= source target))
    s
    (-> s (.getBytes source) (String. target))))

(def ->UTF-8 (partial recode-string "UTF-8"))

(defn clean-row
  "Clean row vectors

  Cleaning entails:
  - String trimming
  - String replacing tabs, newlines and any character used as separator - replacing them with a space.
  - Boolean replacement with numeric equivalent (1/0)
  "
  [regex {:keys [bigdec->str max-str-bytes encoding]} input-row]
  (vec
   (map
    #(cond
       (string? %)
       (-> (->UTF-8 encoding %)
           (string/replace regex " ")
           (.replaceAll "(^\\h*)|(\\h*$)" " ")
           string/trim
           (shorten-str max-str-bytes))
       (and bigdec->str (= (type %) BigDecimal))
       (trn/bigdecimal->plainString %)
       :else %)
    input-row)))

(defn clean-row-fn [{:keys [separator separators] :as clean-config}]
  (partial clean-row (apply clean-regex (concat (when separator [separator]) separators)) clean-config))

(defn data->reducer-fn
  [{:keys [separator reducer-fn reducer-fn-out columns reducer-fn-returns-ds] :as execute-opts
    :or {reducer-fn-returns-ds false}}]
  (let [clean (clean-row-fn {:separator separator})
        lc-columns (map (comp keyword string/lower-case name) (or reducer-fn-out columns))]
    (cond
      (as-arrays? execute-opts)
      (fn [ds r]
        (conj ds (clean r)))
      reducer-fn-returns-ds
      (fn [ds r]
        (if-let [ds1 (not-empty (reducer-fn r))]
          (->> ds1
               (map #((apply juxt lc-columns) %))
               (map clean)
               (concat ds))
          ds))
      :else
      (fn [ds r]
        (if-let [r1 (-> r reducer-fn (#(when % ((apply juxt lc-columns) %))))]
          (conj ds (clean r1))
          ds)))))

(defn data->reducer-fn-file
  [writer separator-char
   {:keys [reducer-fn reducer-fn-out columns reducer-fn-returns-ds max-str-bytes encoding] :as execute-opts
    :or {reducer-fn-returns-ds false}}]
  (let [clean (clean-row-fn {:separator     separator-char
                             :max-str-bytes max-str-bytes
                             :bigdec->str   true
                             :encoding      encoding})
        lc-columns (map (comp keyword string/lower-case name) (or reducer-fn-out columns))]
    (cond
      (as-arrays? execute-opts)
      (fn [_ r]
        (csv/write-csv writer [(clean r)] :separator separator-char))
      reducer-fn-returns-ds
      (fn [_ r]
        (when-let [ds1 (not-empty (reducer-fn r))]
          (->> ds1
               (map #((apply juxt lc-columns) %))
               (map clean)
               (#(csv/write-csv writer % :separator separator-char)))))
      :else
      (fn [_ r]
        (when-let [r1 (-> r reducer-fn (#(when % ((apply juxt lc-columns) %))))]
          (csv/write-csv writer [(clean r1)] :separator separator-char))))))

(defn- format-sql-for-inspection [sql]
  (let [v (if (coll? sql) (first sql) sql)]
    (->> (string/split v #"\n")
         (map (comp string/lower-case string/trim))
         ; remove comments
         (filter #(not (string/starts-with? % "--")))
         (string/join " "))))

(def known-ddl-objects "(index|table|database|view|external|statistics)")
(def known-ddl-operations ["alter" "create" "drop" "update"])
(def known-ddl (map #(str % "\\s+([a-z]+\\s+)?" known-ddl-objects) known-ddl-operations))

(defn- sql-known-ddl? [sql]
  (boolean (some #(re-find (re-pattern %) sql) known-ddl)))

(defn sql-is-ddl? [sql]
  (-> sql format-sql-for-inspection sql-known-ddl?))