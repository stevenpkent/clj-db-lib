(ns clj-db-lib.provider-test
  (:require [clojure.test :refer [deftest is testing]]
            [clj-db-lib.spec.connections :as c]
            [clj-db-lib.provider :refer [db-spec session-queries]]))

(deftest test-db-spec
  (testing "SQL Server connection from properties"
    (is (= (db-spec {::c/provider :mssql
                     ::c/host     "localhost"
                     ::c/user     "sa"
                     ::c/pswd     "secret"
                     ::c/port     1434
                     ::c/dbname   "test"})
           {:adapter       "sqlserver"
            :server-name   "localhost"
            :database-name "test"}))
    (is (= (db-spec {::c/provider :mssql
                     ::c/host     "localhost"
                     ::c/dbname   "test"})
           {:jdbc-url "jdbc:sqlserver://localhost:1433;database=test;integratedSecurity=true"}))
    (is (= (db-spec {::c/provider :mssql
                     ::c/host     "localhost"
                     ::c/port     1434
                     ::c/dbname   "test"})
           {:jdbc-url    "jdbc:sqlserver://localhost:1434;database=test;integratedSecurity=true"}))
    (is (= (db-spec {::c/provider :synapse
                     ::c/host     "localhost"
                     ::c/port     1434
                     ::c/dbname   "test"})
           {:jdbc-url    "jdbc:sqlserver://localhost:1434;database=test;integratedSecurity=true"}))
    (is (= (db-spec {::c/provider    :mssql
                     ::c/host        "localhost"
                     ::c/dbname      "test"
                     ::c/port        1433
                     ::c/auth_scheme "javaKerberos"})
           {:jdbc-url    "jdbc:sqlserver://localhost:1433;database=test;integratedSecurity=true;authenticationScheme=javaKerberos"})))
  (testing "Oracle connection from properties"
    (is (= (db-spec {::c/provider :oracle
                     ::c/host     "localhost"
                     ::c/dbname   "test"
                     ::c/user     "sa"
                     ::c/pswd     "secret"
                     ::c/port     1521
                     ::c/schema   "dbo"})
           {:jdbc-url    "jdbc:oracle:thin:@//localhost:1521/test"}))
    (is (= (db-spec {::c/provider :oracle
                     ::c/host     "localhost"
                     ::c/dbname   "test"
                     ::c/user     "sa"
                     ::c/pswd     "secret"})
           {:jdbc-url "jdbc:oracle:thin:@//localhost:1521/test"}))
    (is (= (db-spec {::c/provider :oracle
                     ::c/host     "localhost"
                     ::c/user     "sa"
                     ::c/pswd     "secret"
                     ::c/sid      "sid"})
           {:jdbc-url "jdbc:oracle:thin:@localhost:1521:sid"})))
  (testing "H2 connection from properties"
    (is (= (db-spec {::c/provider :h2
                     ::c/user     "sa"
                     ::c/password "secret"})
           {:adapter "h2"
            :url     "jdbc:h2:mem:demo;DB_CLOSE_DELAY=-1"}))
    (is (= (db-spec {::c/provider :h2
                     ::c/dbname   "test"
                     ::c/user     "sa"
                     ::c/password "secret"})
           {:adapter "h2"
            :url     "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"}))
    (is (= (db-spec {::c/provider :h2
                     ::c/h2_path  "/some/db/here"
                     ::c/user     "sa"
                     ::c/password "secret"})
           {:adapter "h2"
            :url     "jdbc:h2:file:/some/db/here"})))
  (testing "Unsupported provider"
    (is (thrown? Exception (db-spec {::c/provider :mangodb
                                     ::c/host     "localhost"
                                     ::c/user     "sa"
                                     ::c/pswd     "secret"
                                     ::c/dbname   "mango"})))))

(deftest test-session-queries
  (testing "Retrieving session queries"
    (is (pos? (count (session-queries {::c/provider :oracle}))))))